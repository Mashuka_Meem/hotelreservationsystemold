﻿using System.Web;
using System.Web.Optimization;

namespace HotelReservationSystem
{
    public class BundleConfig
    {
        // For more information on bundling, visit https://go.microsoft.com/fwlink/?LinkId=301862
        public static void RegisterBundles(BundleCollection bundles)
        {
            bundles.Add(new ScriptBundle("~/bundles/jquery").Include(
                        "~/Scripts/jquery-{version}.js"));

            bundles.Add(new ScriptBundle("~/bundles/jqueryval").Include(
                        "~/Scripts/jquery.validate*"));

            // Use the development version of Modernizr to develop with and learn from. Then, when you're
            // ready for production, use the build tool at https://modernizr.com to pick only the tests you need.
            bundles.Add(new ScriptBundle("~/bundles/modernizr").Include(
                        "~/Scripts/modernizr-*"));

            bundles.Add(new ScriptBundle("~/bundles/bootstrap").Include(
                      "~/Scripts/bootstrap.js"));

            bundles.Add(new StyleBundle("~/Content/css").Include(
                      "~/Content/bootstrap.css",
                      "~/Content/fontawesome.css",
                      "~/Content/site.css"));

            bundles.Add(new StyleBundle("~/Style/css").Include(
                      "~/Style/common.css",
                      "~/Style/index.css",
                      "~/Style/dashboard.css",
                      "~/Style/Admin/commonAdmin.css",
                      "~/Style/Admin/adminDashboard.css",
                      "~/Style/User/commonUser.css",
                      "~/Style/User/userDashboard.css",
                      "~/Style/User/hotels.css",
                      "~/Style/User/hotelDetails.css",
                      "~/Style/User/reserveRoom.css",
                      "~/Style/User/reservations.css",
                      "~/Style/footer.css",
                      "~/Style/test.css"));
        }
    }
}
