﻿using HotelReservationSystem.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace HotelReservationSystem.ViewModels
{
    public class HomeHotelSearchVM
    {
        public IEnumerable<Hotel> Hotel { get; set; }
        public IEnumerable<Room> Room { get; set; }
        public IEnumerable<Reservation> Reservation { get; set; } 
    }
}