﻿using HotelReservationSystem.Models;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace HotelReservationSystem.ViewModels
{
    public class SearchViewModel
    {
        //public Reservation Reservations { get; set; }

        //public IEnumerable<Room> Rooms { get; set; }

        public int Id { get; set; }

        public int RoomId { get; set; }

        public virtual Room Room { get; set; }

        [Display(Name = "Check In Date")]
        public Nullable<DateTime> CheckIn { get; set; }

        [Display(Name = "Check Out Edit")]
        public Nullable<DateTime> CheckOut { get; set; }

        public Nullable<int> RoomNo { get; set; }

        //public Nullable<int> UserId { get; set; }

        //public virtual User User { get; set; }

        [Display(Name = "Guest Name")]
        public string GuestName { get; set; }

        [Display(Name = "Guest Contact No")]
        public string Contact { get; set; }

        [Display(Name = "Total Amount")]
        public Nullable<int> TotalAmount { get; set; }



        

        public Nullable<int> HotelId { get; set; }

        public virtual Hotel Hotel { get; set; }

        //public string Description { get; set; }

        //public string Image { get; set; }

        //public Nullable<int> RoomTypeId { get; set; }

        //public virtual RoomType RoomType { get; set; }

        public Nullable<int> TotalRoom { get; set; }

        public Nullable<int> AvailableRoom { get; set; }

        //public string RoomAmenities { get; set; }

        //public Nullable<int> Price { get; set; }





        [Display(Name = "Hotel Name")]
        public string Name { get; set; }

        public string Description { get; set; }

        public string Image { get; set; }

        public string Email { get; set; }


        [Display(Name = "Road Address")]
        public string RoadAddress { get; set; }

        public Nullable<int> AreaId { get; set; }

        public virtual Area Area { get; set; }

        public Nullable<int> DistrictId { get; set; }

        public virtual District District { get; set; }

        public Nullable<int> DivisionId { get; set; }

        public virtual Division Division { get; set; }

        public Nullable<int> HotelRatingId { get; set; }

        public virtual HotelRating HotelRating { get; set; }

        public string HotelAmenities { get; set; }

        public virtual HotelAmenity HotelAmenity { get; set; }

        public string Password { get; set; }

        public Nullable<int> ActorId { get; set; }

        public virtual Actor Actor { get; set; }

        public Nullable<bool> Status { get; set; }
    }
}