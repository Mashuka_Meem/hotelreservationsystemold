﻿using HotelReservationSystem.Models;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.IO;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;

namespace HotelReservationSystem.Controllers
{
    public class HotelController : Controller
    {
        private DatabaseContext db = new DatabaseContext();

        //GET: HotelAccount
        public ActionResult AccountList()
        {
            return View(db.HotelAccount.ToList());
        }

        //GET: HotelAccount/Create
        public ActionResult AccountListPartial()
        {
            HotelAccount hotelAccount = new HotelAccount();
            using (DatabaseContext db = new DatabaseContext())
            {
                hotelAccount.PaymentMethodIds = db.PaymentMethod.ToList();
            }

            return View(hotelAccount);
        }

        //POST: HotelAccount/Create
        public ActionResult CreateHotelAccount(HotelAccount hotelAccount)
        {
            hotelAccount.EditDate = @DateTime.Now;
            hotelAccount.HotelId = 1;           

            if (ModelState.IsValid)
            {
                db.HotelAccount.Add(hotelAccount);
                db.SaveChanges();
                return RedirectToAction("AccountList");
            }

            return View(hotelAccount);
        }

        //GET: HotelAccount/Edit/1
        public ActionResult EditHotelAccount(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            HotelAccount hotelAccount = db.HotelAccount.Find(id);
            if (hotelAccount == null)
            {
                return HttpNotFound();
            }
            return View(hotelAccount);
        }

        //POST: HotelAccount/Edit/1
        [HttpPost]
        public ActionResult EditHotelAccount(HotelAccount hotelAccount)
        {
            //admin.JoinDate = @DateTime.Now;
            hotelAccount.EditDate = DateTime.Now;
            hotelAccount.HotelId = 1;

            if (ModelState.IsValid)
            {
                db.Entry(hotelAccount).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("AccountList");
            }
            return View(hotelAccount);
        }

        //GET: HotelAccount/Delete/1
        public ActionResult DeleteHotelAccount(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            HotelAccount hotelAccount = db.HotelAccount.Find(id);
            if (hotelAccount == null)
            {
                return HttpNotFound();
            }
            return View(hotelAccount);
        }

        //POST: HotelAccount/Delete/1
        [HttpPost]
        public ActionResult DeleteHotelAccount(int id)
        {
            HotelAccount hotelAccount = db.HotelAccount.Find(id);
            db.HotelAccount.Remove(hotelAccount);
            db.SaveChanges();
            return RedirectToAction("AccountList");
        }
    
        //View Confirm Reservation
        public ActionResult ConfirmReservation()
        {
            return View(db.Reservation.ToList());
        }

        //GET: Hotel
        public ActionResult HotelDashboard()
        {
            var test = db.Hotel.FirstOrDefault();

            Hotel model = new Hotel
            {
                Id = test.Id,
                Name = test.Name,
                Description = test.Description,
                Image = test.Image,
                Contact = test.Contact,
                Email = test.Email,
                RoadAddress = test.RoadAddress,
                AreaId = test.AreaId,
                DistrictId = test.DistrictId,
                DivisionId = test.DivisionId,
                HotelRatingId = test.HotelRatingId,
                HotelAmenities = test.HotelAmenities,
                Password = test.Password,
                ActorId = test.ActorId,
                Status = test.Status,
                JoinDate = test.JoinDate,
                EditDate = test.EditDate
            };
            using (DatabaseContext db = new DatabaseContext())
            {

                //model.HotelAmenityIdsArray = model.HotelAmenities.Split(',').ToArray();

                model.HotelAmenityIds = db.HotelAmenity.ToList();
                model.HotelRatingIds = db.HotelRating.ToList();
                model.DivisionIds = db.Division.ToList();
                model.DistrictIds = db.District.ToList();
                model.AreaIds = db.Area.ToList();
            }

            return View(model);
        }

        //POST: Hotel/Details/1
        [HttpPost]
        public ActionResult HotelDashboard(Hotel hotel)
        {
            var data = db.Hotel.Where(x => x.Email == hotel.Email && x.Password == hotel.Password).FirstOrDefault();
            if (data == null)
            {
                return RedirectToAction("Index");
            }
            else
            {
                //data.HotelAmenityIdsArray = data.HotelAmenities.Split(',').ToArray();
                Session["Id"] = hotel.Id;
                Session["Email"] = hotel.Email;
                return View(data);
            }
            //return View();
        }

        //GET: Hotel/Edit/1
        public ActionResult EditHotel(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Hotel user = db.Hotel.Find(id);
            if (user == null)
            {
                return HttpNotFound();
            }
            return View(user);
        }

        //POST: Hotel/Edit/1
        [HttpPost]
        public ActionResult EditHotel(Hotel hotel, HttpPostedFileBase Image)
        {
            //admin.JoinDate = @DateTime.Now;
            hotel.EditDate = DateTime.Now;

            //if (Image.ContentLength > 0)
            if (Image != null)
            {
                string fileName = Path.GetFileNameWithoutExtension(Image.FileName);
                string extension = Path.GetExtension(Image.FileName);
                fileName = DateTime.Now.ToString("yymmssfff") + fileName + extension;
                hotel.Image = "~/Images/" + fileName;
                string path = Path.Combine(Server.MapPath("~/Images/"), fileName);
                Image.SaveAs(path);
            }
            hotel.HotelAmenities = string.Join(",", hotel.HotelAmenityIdsArray);
            if (ModelState.IsValid)
            {
                db.Entry(hotel).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("HotelDashboard");
            }
            return View(hotel);
        }

        // GET: Hotel
        public ActionResult Index()
        {
            return View();
        }


        //GET: Reservation
        public ActionResult ReservationList()
        {
            return View(db.Reservation.ToList());
        }

        //GET: Reservation/Delete/1
        public ActionResult DeleteReservation(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Reservation reservation = db.Reservation.Find(id);
            if (reservation == null)
            {
                return HttpNotFound();
            }
            return View(reservation);
        }

        //POST: Reservation/Delete/1
        [HttpPost]
        public ActionResult DeleteReservation(int id)
        {
            Reservation reservation = db.Reservation.Find(id);
            db.Reservation.Remove(reservation);
            db.SaveChanges();
            return RedirectToAction("ReservationList");
        }

        //GET: Room
        public ActionResult RoomList()
        {
            return View(db.Room.ToList());
        }
        
        public ActionResult RoomListPartial() 
        {
            Room room = new Room();
            using (DatabaseContext db = new DatabaseContext())
            {
                room.RoomTypeIds = db.RoomType.ToList();
                room.RoomAmenityIds = db.RoomAmenity.ToList();
            }

            return View(room);
        }

        //POST: Room/Create
        public ActionResult CreateRoom( Room room, HttpPostedFileBase Image)
        {
            room.EditDate = @DateTime.Now;
            room.HotelId = 1;
            if (Image.ContentLength > 0)
            {
                string fileName = Path.GetFileNameWithoutExtension(Image.FileName);
                string extension = Path.GetExtension(Image.FileName);
                fileName = DateTime.Now.ToString("yymmssfff") + fileName + extension;
                room.Image = "~/Images/" + fileName;
                string path = Path.Combine(Server.MapPath("~/Images/"), fileName);
                Image.SaveAs(path);
            }
           
            room.RoomAmenities = string.Join(",", room.RoomAmenityIdsArray);
            room.AvailableRoom = room.TotalRoom;

            if (ModelState.IsValid)
            {
                db.Room.Add(room);
                db.SaveChanges();
                return RedirectToAction("RoomList");
            }

            return View(room);
        }

        //GET: Room/Edit/1
        public ActionResult EditRoom(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Room room = db.Room.Find(id);
            if (room == null)
            {
                return HttpNotFound();
            }
            return View(room);
        }

        //POST: Room/Edit/1
        [HttpPost]
        public ActionResult EditRoom(Room room, HttpPostedFileBase Image)
        {
            //admin.JoinDate = @DateTime.Now;
            room.EditDate = DateTime.Now;
            room.HotelId = 1;

            if (Image.ContentLength > 0)
            {
                string fileName = Path.GetFileNameWithoutExtension(Image.FileName);
                string extension = Path.GetExtension(Image.FileName);
                fileName = DateTime.Now.ToString("yymmssfff") + fileName + extension;
                room.Image = "~/Images/" + fileName;
                string path = Path.Combine(Server.MapPath("~/Images/"), fileName);
                Image.SaveAs(path);
            }

            room.RoomAmenities = string.Join(",", room.RoomAmenityIdsArray);

            if (ModelState.IsValid)
            {
                db.Entry(room).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("RoomList");
            }
            return View(room);
        }

        //GET: Room/Delete/1
        public ActionResult DeleteRoom(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Room room = db.Room.Find(id);
            if (room == null)
            {
                return HttpNotFound();
            }
            return View(room);
        }

        //POST: Room/Delete/1
        [HttpPost]
        public ActionResult DeleteRoom(int id)
        {
            Room room = db.Room.Find(id);
            db.Room.Remove(room);
            db.SaveChanges();
            return RedirectToAction("RoomList");
        }

        //GET: Room/Block/1
        public ActionResult BlockRoom(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Room room = db.Room.Find(id);
            if (room == null)
            {
                return HttpNotFound();
            }
            return View(room);
        }

        //POST: Room/Block/1
        [HttpPost]
        public ActionResult BlockRoom(int id)
        {
            Room room = db.Room.Find(id);
            if (ModelState.IsValid)
            {
                room.Status = false;
                db.Entry(room).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("RoomList");
            }
            return View(room);
        }

        //GET: Room/Unblock/1
        public ActionResult UnblockRoom(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Room room = db.Room.Find(id);
            if (room == null)
            {
                return HttpNotFound();
            }
            return View(room);
        }

        //POST: Room/Unblock/1
        [HttpPost]
        public ActionResult UnblockRoom(int id)
        {
            Room room = db.Room.Find(id);
            if (ModelState.IsValid)
            {
                room.Status = true;
                db.Entry(room).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("RoomList");
            }
            return View(room);
        }

        //POST: Login
        [HttpPost]
        public ActionResult Login(Hotel hotel)
        {
            var data = db.Hotel.Where(x => x.Email == hotel.Email && x.Password == hotel.Password).FirstOrDefault();
            if (data == null)
            {
                return View("Index", hotel);
            }
            else
            {
                Session["Id"] = hotel.Id;
                Session["Email"] = hotel.Email;
                return RedirectToAction("HotelDashboard");
            }
            //return View();
        }

        //GET: Logout
        public ActionResult LogOut()
        {
            int Id = (int)Session["Id"];
            Session.Abandon();
            return RedirectToAction("Index");
        }
    }
}