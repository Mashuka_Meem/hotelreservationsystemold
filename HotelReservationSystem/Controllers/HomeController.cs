﻿using HotelReservationSystem.Models;
using HotelReservationSystem.ViewModels;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.IO;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;

namespace HotelReservationSystem.Controllers
{
    public class HomeController : Controller
    {
        private DatabaseContext db = new DatabaseContext();

        //View About
        public ActionResult About()
        {
            ViewBag.Message = "Your application description page.";

            return View();
        }
        

        //View Contact
        public ActionResult Contact()
        {
            ViewBag.Message = "Your contact page.";

            return View();
        }

        //GET: User
        public ActionResult Dashboard()
        {
            var test = db.User.FirstOrDefault();

            User model = new User
            {
                Id = test.Id,
                Name = test.Name,
                Image = test.Image,
                Contact = test.Contact,
                Email = test.Email,
                Password = test.Password,
                ActorId = test.ActorId,
                JoinDate = test.JoinDate,
                EditDate = test.EditDate,
                Status = test.Status
            };
            return View(model);
        }          

        //POST: User/Details/1
        [HttpPost]
        public ActionResult Dashboard(User user)
        {
            var data = db.User.Where(x => x.Email == user.Email && x.Password == user.Password).FirstOrDefault();
            if (data == null)
            {
                return RedirectToAction("Index");
            }
            else
            {
                Session["Id"] = user.Id;
                Session["Email"] = user.Email;
                return View(data);
            }
            //return View();
        }
        //public ActionResult Dashboard(int? id)
        //{
        //    if (id == null)
        //    {
        //        return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
        //    }
        //    User user = db.User.Find(id);
        //    if (user == null)
        //    {
        //        return HttpNotFound();
        //    }
        //    return View(user);
        //}

        //GET: User/Edit/1
        public ActionResult EditUser(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            User user = db.User.Find(id);
            if (user == null)
            {
                return HttpNotFound();
            }
            return View(user);
        }

        //POST: User/Edit/1
        [HttpPost]
        public ActionResult EditUser([Bind(Include = "Id, Name, Image, Email, Contact, Password, ActorId, Status, JoinDate, EditDate")] User user, HttpPostedFileBase Image)
        {
            //admin.JoinDate = @DateTime.Now;
            user.EditDate = DateTime.Now;

            if (Image.ContentLength > 0)
            {
                string fileName = Path.GetFileNameWithoutExtension(Image.FileName);
                string extension = Path.GetExtension(Image.FileName);
                fileName = DateTime.Now.ToString("yymmssfff") + fileName + extension;
                user.Image = "~/Images/" + fileName;
                string path = Path.Combine(Server.MapPath("~/Images/"), fileName);
                Image.SaveAs(path);
            }

            if (ModelState.IsValid)
            {
                db.Entry(user).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Dashboard");
            }
            return View(user);            
        }

        //GET: Hotel/Details/1
        public ActionResult HotelDetails(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Hotel hotel = db.Hotel.Find(id);
            if (hotel == null)
            {
                return HttpNotFound();
            }
            return View(hotel);
        }

        ////GET: HotelDetailsPartial
        //public ActionResult HotelDetailsPartial()
        //{
        //    return View(db.Room.ToList());
        //}

        //GET: HotelDetailsPartial
        public ActionResult HotelDetailsPartial(int? id)
        {
            return View(db.Room.Where(x => x.HotelId == id).ToList());
        }


        //View Hotels
        public ActionResult Hotels()
        {
            return View(db.Hotel.ToList()); 
        }

        ////GET: Hotel
        //public ActionResult HotelSearch(string name, Nullable<DateTime> checkIn, Nullable<DateTime> checkOut) 
        //{
        //    return View(db.Reservation.Where(x => x.Room.Hotel.Name.Contains(name) && x.CheckIn == checkIn && x.CheckOut == checkOut || x.Room.Hotel.Area.Name.Contains(name) && x.CheckIn == checkIn && x.CheckOut == checkOut || x.Room.Hotel.District.Name.Contains(name) && x.CheckIn == checkIn && x.CheckOut == checkOut).ToList());
        //}

        //GET: HotelSearch
        //public ActionResult HotelSearch(string name, Nullable<DateTime> checkIn, Nullable<DateTime> checkOut, Nullable<int> roomNo, SearchViewModel searchViewModel)
        //{
        //    //var data = db.Hotel.Where(x => x.Name.Contains(name) || x.Area.Name.Contains(name) || x.District.Name.Contains(name)).ToList();
        //    Hotel hotel = new Hotel();
        //    Reservation reservation = new Reservation();
        //    Room room = new Room();

        //    var dataHotel = db.Hotel.Where(x => x.Name.Contains(name) || x.Area.Name.Contains(name) || x.District.Name.Contains(name)).ToList();

        //    if (dataHotel != null)
        //    {
        //        var dataRoom = db.Room.Where(y => y.AvailableRoom != 0 && y.AvailableRoom >= roomNo).ToList();

        //        if (dataRoom != null)
        //        {
        //            var dataReservation = db.Reservation.Where(z => z.CheckIn == checkIn && z.CheckOut == checkOut).ToList();

        //            if (dataReservation != null)
        //            {
        //                return View(db.Hotel.ToList());
        //            }
        //        }
        //    }

        //    //else
        //    //{
        //    //    return HttpNotFound();
        //    //}
        //    return View(db.Hotel.ToList());
        //}

        //GET: HotelSearch
        
        public ActionResult HotelSearch(string name, Nullable<DateTime> checkIn, Nullable<DateTime> checkOut, Nullable<int> roomNo)
        {
            Hotel hotel = new Hotel();
            Reservation reservation = new Reservation();
            Room room = new Room();

            var dataHotel = db.Hotel.Where(x => x.Name.Contains(name) || x.Area.Name.Contains(name) || x.District.Name.Contains(name)).ToList();

            if (dataHotel != null)
            {
                foreach (var a in dataHotel)
                {
                    if (room.AvailableRoom !=0 )
                    {
                        var dataRoom = db.Room.Where(y => y.AvailableRoom != 0 && y.AvailableRoom >= roomNo).ToList();

                        if (dataRoom != null)
                        {
                            foreach (var b in dataRoom)
                            {
                                //if(reservation.RoomId == b.Id)
                                //{
                                //var dataReservation = db.Reservation.Where(z => z.CheckIn == checkIn && z.CheckOut == checkOut).ToList();

                                //if (dataReservation.Count() != 0)
                                //{
                                //    RedirectToAction("Index");
                                //}
                                //else
                                //{
                                //foreach (var c in dataReservation)
                                //    {
                                var vm = new HomeHotelSearchVM();

                                vm.Hotel = dataHotel;
                                //vm.Reservation = dataReservation;
                                vm.Room = dataRoom;

                                return View(vm);
                                //}
                                //}
                                //}

                            }

                        }
                    }
                    else if (room.AvailableRoom == 0)
                    {

                  
                                //if(reservation.RoomId == b.Id)
                                //{
                                //var dataReservation = db.Reservation.Where(z => z.CheckIn == checkIn && z.CheckOut == checkOut).ToList();
                                var dataReservation = db.Reservation.Where(z => z.CheckIn == checkIn).Where(z => z.CheckOut == checkOut).ToList();

                                //if (dataReservation.Count() != 0)
                                //{
                                //    RedirectToAction("Index");
                                //}
                                //else
                                //{
                                if (dataReservation != null)
                        {
                            foreach (var c in dataReservation)
                            {
                                //int? num = c.RoomNo;
                                //room.AvailableRoom = room.AvailableRoom + num;

                                //if (room.AvailableRoom > 0)
                                //{
                                //    var dataRoom1 = db.Room.Where(y => y.AvailableRoom >= roomNo).ToList();


                                //    foreach (var d in dataRoom1)
                                //    {
                                        var vm = new HomeHotelSearchVM();

                                        vm.Hotel = dataHotel;
                                        vm.Reservation = dataReservation;
                                        //vm.Room = dataRoom1;

                                        return View(vm);
                                //    }
                                //}
                                    
                            }
                        }
                                //}
                            }


                        //}
                    }
                    


                    

                
            }

            //else
            //{
            //    return HttpNotFound();
            //}
            return View();
        }

        //GET: Home
        public ActionResult Index(string name, Nullable<DateTime> checkIn, Nullable<DateTime> checkOut)
        {
            return View(db.Reservation.Where(x => x.Room.Hotel.Name.Contains(name) && x.CheckIn == checkIn && x.CheckOut == checkOut || x.Room.Hotel.Area.Name.Contains(name) && x.CheckIn == checkIn && x.CheckOut == checkOut || x.Room.Hotel.District.Name.Contains(name) && x.CheckIn == checkIn && x.CheckOut == checkOut).ToList());
        }

        //View PendingReservation List
        public ActionResult PendingReservation()
        {
            return View();
        }       

        //View Reservations
        public ActionResult Reservations()
        {
            return View(db.Reservation.ToList());
        }

        //GET: ReserveRoom
        public ActionResult ReserveRoom(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            //Room room = db.Room.Find(id);
            Room room = db.Room.Find(id);
            if (room == null)
            {
                return HttpNotFound();
            }
            return View(room);
        }

        ////POST: Reservation/Create 
        //[HttpPost]
        //public ActionResult CreateReservation([Bind(Include = "Id, RoomId, CheckIn, CheckOut, RoomNo, UserId, GuestName, Contact, TotalAmount, Status, AddDate")] Reservation reservation)
        //{
        //    reservation.AddDate = @DateTime.Now;
        //    reservation.Status = false;
        //    //reservation.RoomId = 2;
        //    reservation.UserId = 1;

        //    if (ModelState.IsValid)
        //    {
        //        db.Reservation.Add(reservation);
        //        db.SaveChanges();
        //        return RedirectToAction("PendingReservation");
        //    }

        //    return View(reservation);
        //}


        //POST: Reservation/Create 
        [HttpPost]
        public ActionResult CreateReservation(SearchViewModel searchViewModel)
        {
            Reservation reservation = new Reservation();
            reservation.AddDate = @DateTime.Now;
            reservation.Status = false;
            reservation.UserId = 1;

            reservation.GuestName = searchViewModel.GuestName;
            reservation.Contact = searchViewModel.Contact;
            reservation.CheckIn = searchViewModel.CheckIn;
            reservation.CheckOut = searchViewModel.CheckOut;
            reservation.RoomNo = searchViewModel.RoomNo;
            reservation.RoomId = searchViewModel.RoomId;
            reservation.TotalAmount = searchViewModel.TotalAmount;

            Room room = new Room();
            room.Id = searchViewModel.RoomId;
            room = db.Room.Find(room.Id);
            room.AvailableRoom = searchViewModel.AvailableRoom;
            room.AvailableRoom = room.AvailableRoom - reservation.RoomNo;

            if (ModelState.IsValid)
            {
                db.Reservation.Add(reservation);
                db.Entry(room).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("PendingReservation");
            }


            return View(searchViewModel);
        }

        //View Test
        public ActionResult Test()
        {
            Room room = new Room();
            using (DatabaseContext db = new DatabaseContext())
            {
                room.RoomTypeIds = db.RoomType.ToList();

            }

            return View(room);
        }

        //View Test
        //public ActionResult AddorEdit(int id = 0)
        //{

        //    Room room = new Room();
        //    using (DatabaseContext db = new DatabaseContext()){
        //        room.RoomTypeIds = db.RoomType.ToList();

        //    }

        //    return View(room);
        //}       

        //POST: Test Room/Create
        //public ActionResult AddorEdit(Room room)
        //{
        //    room.HotelId = 2;
        //    room.Status = true;
        //    room.EditDate = @DateTime.Now;

        //    room.RoomTypes = string.Join(", ", room.RoomTypeIdsArray);

        //    using (DatabaseContext db = new DatabaseContext())
        //    {
        //        if (room.Id == 0)
        //        {
        //            db.Room.Add(room);
        //        }
        //        //else
        //        //{
        //        //    db.Entry(room).State = EntityState.Modified;
        //        //}

        //        db.SaveChanges();
        //    }
        //    return RedirectToAction("Index");
        //}        

        //POST: Register/Create 
        [HttpPost]
        public ActionResult CreateRegistration([Bind(Include = "Id, Name, Image, Email, Contact, Password, ActorId, Status, JoinDate, EditDate")] User user)
        {
            user.ActorId = 1;
            user.Status = true;
            user.JoinDate = @DateTime.Now;
            user.EditDate = @DateTime.Now;

            if (ModelState.IsValid)
            {
                db.User.Add(user);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            return View(user);
        }

        //POST: Login
        [HttpPost]
        public ActionResult Login(User user)
        {
            var data = db.User.Where(x => x.Email == user.Email && x.Password == user.Password).FirstOrDefault();
            if (data == null)
            {
                return View("Index", user);
            }
            else
            {
                Session["Id"] = user.Id;
                Session["Email"] = user.Email;
                return RedirectToAction("Dashboard");
            }
            //return View();
        }

        //GET: Logout
        public ActionResult LogOut()
        {
            int Id = (int)Session["Id"];
            Session.Abandon();
            return RedirectToAction("Index");
        }
    }
}