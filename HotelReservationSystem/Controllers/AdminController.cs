﻿using HotelReservationSystem.Models;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.IO;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Hosting;
using System.Web.Mvc;

namespace HotelReservationSystem.Controllers
{
    public class AdminController : Controller
    {
        private DatabaseContext db = new DatabaseContext();

        //GET: Actor 
        public ActionResult ActorList(string searching)
        {
            return View(db.Actor.Where(x => x.Type.Contains(searching) || searching == null).ToList());
        }

        ////GET: Actor 
        //public ActionResult SearchActor(string searching)
        //{
            
        //}

        //POST: Actor/Create 
        [HttpPost]
        public ActionResult CreateActor([Bind(Include = "Id, Type, EditDate")] Actor actor)
        {           
            actor.EditDate = @DateTime.Now;

            if (ModelState.IsValid)
            {
                db.Actor.Add(actor);
                db.SaveChanges();
                return RedirectToAction("ActorList");
            }

            return View(actor);
        }

        //GET: Actor/Edit/1
        public ActionResult EditActor(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Actor actor = db.Actor.Find(id);
            if (actor == null)
            {
                return HttpNotFound();
            }
            return View();
        }       

        //POST: Actor/Edit/1
        [HttpPost]
        public ActionResult EditActor([Bind(Include = "Id, Type, EditDate")] Actor actor)
        {
            actor.EditDate = @DateTime.Now;

            if (ModelState.IsValid)
            {
                db.Entry(actor).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("ActorList");
            }
            return View(actor);
        }

        // GET: Actor/Delete/1
        public ActionResult DeleteActor(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Actor actor = db.Actor.Find(id);
            if (actor == null)
            {
                return HttpNotFound();
            }
            return View(actor);
        }

        // POST: Actor/Delete/1
        [HttpPost]
        public ActionResult DeleteActor(int id)
        {
            Actor actor = db.Actor.Find(id);
            db.Actor.Remove(actor);
            db.SaveChanges();
            return RedirectToAction("ActorList");
        }

        //GET: Admin/Details/1
        public ActionResult AdminDashboard()
        {
            var test = db.Admin.FirstOrDefault();

            Admin model = new Admin
            {
                Id = test.Id,
                Name = test.Name,
                Image = test.Image,
                Contact = test.Contact,
                Email = test.Email,
                Password = test.Password,
                ActorId = test.ActorId,
                JoinDate = test.JoinDate,
                EditDate = test.EditDate,
                Status = test.Status
            };
            return View(model);
        }

        // POST: Admin/Details/1
        [HttpPost]
        public ActionResult AdminDashboard(Admin admin) 
        {
            var test = db.Admin.Where(x => x.Id == admin.Id && x.Password == admin.Password).FirstOrDefault();
            if (test == null)
            {
                return RedirectToAction("Index");
            }
            else
            {
                Session["Id"] = admin.Id;
                Session["Email"] = admin.Email;
                return View(test);
            }
            //return View();
        }

        //GET: Admin/Edit/1
        public ActionResult EditAdmin(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Admin admin = db.Admin.Find(id);
            if (admin == null)
            {
                return HttpNotFound();
            }
            return View(admin);
        }

        //POST: Admin/Edit/1
        [HttpPost]
        public ActionResult EditAdmin([Bind(Include = "Id, Name, Image, Email, Contact, Password, ActorId, Status, JoinDate, EditDate")] Admin admin, HttpPostedFileBase Image)
        {
            //admin.JoinDate = @DateTime.Now;
            admin.EditDate = DateTime.Now;

            if (Image.ContentLength > 0)
            {
                string fileName = Path.GetFileNameWithoutExtension(Image.FileName);
                string extension = Path.GetExtension(Image.FileName);
                fileName = DateTime.Now.ToString("yymmssfff") + fileName + extension;
                admin.Image = "~/Images/" + fileName;
                string path = Path.Combine(Server.MapPath("~/Images/"), fileName);
                Image.SaveAs(path);
            }

            if (ModelState.IsValid)
            {
                db.Entry(admin).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("AdminDashboard");
            }
            return View(admin);


            //if (ModelState.IsValid)
            //{
            //    var data = db.Admin.Single(a => a.Id == id);
            //    data.Name = admin.Name;
            //    data.Email = admin.Email;
            //    data.Contact = admin.Contact;
            //    data.Image = admin.Image;
            //    data.Password = admin.Password;
            //    db.SaveChanges();
            //    return RedirectToAction("AdminDashboard");
            //}
            //return View(admin);
        }

        //GET: Admin
        public ActionResult AdminList()
        {
            return View(db.Admin.ToList());
        }

        public ActionResult CreateAdmin()
        {
            return View();
        }

        //POST: Admin/Create 
        [HttpPost]
        public ActionResult CreateAdmin([Bind(Include = "Id, Name, Image, Email, Contact, Password, ActorId, Status, JoinDate, EditDate")] Admin admin, HttpPostedFileBase Image)
        {
            admin.ActorId = 1;
            admin.Status = true;
            admin.JoinDate = @DateTime.Now;
            admin.EditDate = @DateTime.Now;

            if (ModelState.IsValid)
            {
                db.Admin.Add(admin);
                db.SaveChanges();
                return RedirectToAction("AdminList");
            }

            return View(admin);
        }

        //GET: Admin/Delete/1
        public ActionResult DeleteAdmin(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Admin admin = db.Admin.Find(id);
            if (admin == null)
            {
                return HttpNotFound();
            }
            return View(admin);
        }

        //POST: Admin/Delete/1
        [HttpPost]
        public ActionResult DeleteAdmin(int id)
        {
            Admin admin = db.Admin.Find(id);
            db.Admin.Remove(admin);
            db.SaveChanges();
            return RedirectToAction("AdminList");
        }

        //GET: Area
        public ActionResult AreaList()
        {
            return View(db.Area.ToList());
        }

        //POST: Area/Create
        [HttpPost]
        public ActionResult CreateArea([Bind(Include = "Id, Name, EditDate")] Area area)
        {
            area.EditDate = @DateTime.Now;

            if (ModelState.IsValid)
            {
                db.Area.Add(area);
                db.SaveChanges();
                return RedirectToAction("AreaList");
            }

            return View(area);
        }

        //GET: Area/Edit/1
        public ActionResult EditArea(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Area area = db.Area.Find(id);
            if (area == null)
            {
                return HttpNotFound();
            }
            return View(area);
        }

        //POST: Area/Edit/1
        [HttpPost]
        public ActionResult EditArea([Bind(Include = "Id, Name, EditDate")] Area area)
        {
            area.EditDate = @DateTime.Now;

            if (ModelState.IsValid)
            {
                db.Entry(area).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("AreaList");
            }
            return View(area);
        }

        // GET: Area/Delete/1
        public ActionResult DeleteArea(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Area area = db.Area.Find(id);
            if (area == null)
            {
                return HttpNotFound();
            }
            return View(area);
        }

        // POST: Area/Delete/1
        [HttpPost]
        public ActionResult DeleteArea(int id)
        {
            Area area = db.Area.Find(id);
            db.Area.Remove(area);
            db.SaveChanges();
            return RedirectToAction("AreaList");
        }

        //GET: District 
        public ActionResult DistrictList()
        {
            return View(db.District.ToList());
        }

        //POST: District/Create
        [HttpPost]
        public ActionResult CreateDistrict([Bind(Include = "Id, Name, EditDate")] District district)
        {
            district.EditDate = @DateTime.Now;

            if (ModelState.IsValid)
            {
                db.District.Add(district);
                db.SaveChanges();
                return RedirectToAction("DistrictList");
            }

            return View(district);
        }

        //GET: District/Edit/1
        public ActionResult EditDistrict(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            District district = db.District.Find(id);
            if (district == null)
            {
                return HttpNotFound();
            }
            return View(district);
        }

        //POST: District/Edit/1
        [HttpPost]
        public ActionResult EditDistrict([Bind(Include = "Id, Name, EditDate")] District district)
        {
            district.EditDate = @DateTime.Now;

            if (ModelState.IsValid)
            {
                db.Entry(district).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("DistrictList");
            }
            return View(district);
        }

        // GET: District/Delete/1
        public ActionResult DeleteDistrict(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            District district = db.District.Find(id);
            if (district == null)
            {
                return HttpNotFound();
            }
            return View(district);
        }

        // POST: District/Delete/1
        [HttpPost]
        public ActionResult DeleteDistrict(int id)
        {
            District district = db.District.Find(id);
            db.District.Remove(district);
            db.SaveChanges();
            return RedirectToAction("DistrictList");
        }

        //GET: Division
        public ActionResult DivisionList()
        {
            return View(db.Division.ToList());
        }

        //POST: Division/Create
        [HttpPost]
        public ActionResult CreateDivision([Bind(Include = "Id, Name, EditDate")] Division division)
        {
            division.EditDate = @DateTime.Now;

            if (ModelState.IsValid)
            {
                db.Division.Add(division);
                db.SaveChanges();
                return RedirectToAction("DivisionList");
            }

            return View(division);
        }

        //GET: Division/Edit/1
        public ActionResult EditDivision(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Division division = db.Division.Find(id);
            if (division == null)
            {
                return HttpNotFound();
            }
            return View(division);
        }

        //POST: Division/Edit/1
        [HttpPost]
        public ActionResult EditDivision([Bind(Include = "Id, Name, EditDate")] Division division)
        {
            division.EditDate = @DateTime.Now;

            if (ModelState.IsValid)
            {
                db.Entry(division).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("DivisionList");
            }
            return View(division);
        }

        // GET: Division/Delete/1
        public ActionResult DeleteDivision(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Division division = db.Division.Find(id);
            if (division == null)
            {
                return HttpNotFound();
            }
            return View(division);
        }

        // POST: Division/Delete/1
        [HttpPost]
        public ActionResult DeleteDivision(int id)
        {
            Division division = db.Division.Find(id);
            db.Division.Remove(division);
            db.SaveChanges();
            return RedirectToAction("DivisionList");
        }

        //GET: HotelAmenity
        public ActionResult HotelAmenityList()
        {
            return View(db.HotelAmenity.ToList());
        }

        //POST: HotelAmenity/Create
        [HttpPost]
        public ActionResult CreateHotelAmenity([Bind(Include = "Id, Name, Image, EditDate")] HotelAmenity hotelAmenity, HttpPostedFileBase Image)
        {
            hotelAmenity.EditDate = @DateTime.Now;

            //if (Image.ContentLength > 0)
            if (Image != null)
            {
                string fileName = Path.GetFileNameWithoutExtension(Image.FileName);
                string extension = Path.GetExtension(Image.FileName);
                fileName = DateTime.Now.ToString("yymmssfff") + fileName + extension;
                hotelAmenity.Image = "~/Images/" + fileName;
                string path = Path.Combine(Server.MapPath("~/Images/"), fileName);
                Image.SaveAs(path);
            }

            if (ModelState.IsValid)
            {
                db.HotelAmenity.Add(hotelAmenity);
                db.SaveChanges();
                return RedirectToAction("HotelAmenityList");
            }

            return View(hotelAmenity);
        }

        //GET: HotelAmenity/Edit/1
        public ActionResult EditHotelAmenity(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            HotelAmenity hotelAmenity = db.HotelAmenity.Find(id);
            if (hotelAmenity == null)
            {
                return HttpNotFound();
            }
            return View(hotelAmenity);
        }

        //POST: HotelAmenity/Edit/1
        [HttpPost]
        public ActionResult EditHotelAmenity([Bind(Include = "Id, Name, Image, EditDate")] HotelAmenity hotelAmenity, HttpPostedFileBase Image)
        {
            hotelAmenity.EditDate = @DateTime.Now;

            //if (Image.ContentLength > 0)
            if (Image != null)
            {
                string fileName = Path.GetFileNameWithoutExtension(Image.FileName);
                string extension = Path.GetExtension(Image.FileName);
                fileName = DateTime.Now.ToString("yymmssfff") + fileName + extension;
                hotelAmenity.Image = "~/Images/" + fileName;
                string path = Path.Combine(Server.MapPath("~/Images/"), fileName);
                Image.SaveAs(path);
            }

            if (ModelState.IsValid)
            {
                db.Entry(hotelAmenity).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("HotelAmenityList");
            }
            return View(hotelAmenity);
        }

        // GET: HotelAmenity/Delete/1
        public ActionResult DeleteHotelAmenity(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            HotelAmenity hotelAmenity = db.HotelAmenity.Find(id);
            if (hotelAmenity == null)
            {
                return HttpNotFound();
            }
            return View(hotelAmenity);
        }

        // POST: HotelAmenity/Delete/1
        [HttpPost]
        public ActionResult DeleteHotelAmenity(int id)
        {
            HotelAmenity hotelAmenity = db.HotelAmenity.Find(id);
            db.HotelAmenity.Remove(hotelAmenity);
            db.SaveChanges();
            return RedirectToAction("HotelAmenityList");
        }

        //GET: Hotel
        public ActionResult HotelList()
        {
            return View(db.Hotel.ToList());
        }

        //POST: Hotel/Create
        [HttpPost]
        public ActionResult CreateHotel([Bind(Include = "Id, Name, Description, Image, Email, Contact, RoadAddress, AreaId, DistrictId,DivisionId, HotelRatingId, HotelAmenities, Password, ActorId, Status, HotelAccount_Id, JoinDate, EditDate")] Hotel hotel)
        {
            hotel.ActorId = 1;
            //hotel.AreaId = 1;
            //hotel.DistrictId = 1;
            //hotel.DivisionId = 1;
            //hotel.HotelRatingId = 1;
            hotel.JoinDate = @DateTime.Now;
            hotel.EditDate = @DateTime.Now;         

            if (ModelState.IsValid)
            {
                db.Hotel.Add(hotel);
                db.SaveChanges();
                return RedirectToAction("HotelList");
            }

            return View(hotel);
        }

        // GET: Hotel/Delete/1
        public ActionResult DeleteHotel(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            HotelAmenity hotelAmenity = db.HotelAmenity.Find(id);
            if (hotelAmenity == null)
            {
                return HttpNotFound();
            }
            return View(hotelAmenity);
        }

        // POST: Hotel/Delete/1
        [HttpPost]
        public ActionResult DeleteHotel(int id)
        {
            Hotel hotel = db.Hotel.Find(id);
            db.Hotel.Remove(hotel);
            db.SaveChanges();
            return RedirectToAction("HotelList");
        }

        //GET: HotelRating
        public ActionResult HotelRatingList()
        {
            return View(db.HotelRating.ToList());
        }

        //POST: HotelRating/Create
        [HttpPost]
        public ActionResult CreateHotelRating([Bind(Include = "Id, Rating, Image, EditDate")] HotelRating hotelRating, HttpPostedFileBase Image)
        {
            hotelRating.EditDate = @DateTime.Now;

            //if (Image.ContentLength > 0)
            if (Image != null)
            {
                string fileName = Path.GetFileNameWithoutExtension(Image.FileName);
                string extension = Path.GetExtension(Image.FileName);
                fileName = DateTime.Now.ToString("yymmssfff") + fileName + extension;
                hotelRating.Image = "~/Images/" + fileName;
                string path = Path.Combine(Server.MapPath("~/Images/"), fileName);
                Image.SaveAs(path);
            }

            if (ModelState.IsValid)
            {
                db.HotelRating.Add(hotelRating);
                db.SaveChanges();
                return RedirectToAction("HotelRatingList");
            }

            return View(hotelRating);
        }

        //GET: HotelRating/Edit/1
        public ActionResult EditHotelRating(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            HotelRating hotelRating = db.HotelRating.Find(id);
            if (hotelRating == null)
            {
                return HttpNotFound();
            }
            return View(hotelRating);
        }

        //POST: HotelRating/Edit/1
        [HttpPost]
        public ActionResult EditHotelRating([Bind(Include = "Id, Rating, Image, EditDate")] HotelRating hotelRating, HttpPostedFileBase Image)
        {
            hotelRating.EditDate = @DateTime.Now;

            //if (Image.ContentLength > 0)
            if (Image != null)
            {
                string fileName = Path.GetFileNameWithoutExtension(Image.FileName);
                string extension = Path.GetExtension(Image.FileName);
                fileName = DateTime.Now.ToString("yymmssfff") + fileName + extension;
                hotelRating.Image = "~/Images/" + fileName;
                string path = Path.Combine(Server.MapPath("~/Images/"), fileName);
                Image.SaveAs(path);
            }
             
            if (ModelState.IsValid)
            {
                db.Entry(hotelRating).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("HotelRatingList");
            }
            return View(hotelRating);
        }

        // GET: HotelRating/Delete/1
        public ActionResult DeleteHotelRating(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            HotelRating hotelRating = db.HotelRating.Find(id);
            if (hotelRating == null)
            {
                return HttpNotFound();
            }
            return View(hotelRating);
        }

        // POST: HotelRating/Delete/1
        [HttpPost]
        public ActionResult DeleteHotelRating(int id)
        {
            HotelRating hotelRating = db.HotelRating.Find(id);
            db.HotelRating.Remove(hotelRating);
            db.SaveChanges();
            return RedirectToAction("HotelRatingList");
        }

        // GET: AdminDashboard
        public ActionResult Index()
        {
            return View();
        }

        //GET: PaymentMethod
        public ActionResult PaymentMethodList()
        {
            return View(db.PaymentMethod.ToList());
        }

        //POST: PaymentMethod/Create
        [HttpPost]
        public ActionResult CreatePaymentMethod([Bind(Include = "Id, Method, EditDate")] PaymentMethod paymentMethod)
        {
            paymentMethod.EditDate = @DateTime.Now;

            if (ModelState.IsValid)
            {
                db.PaymentMethod.Add(paymentMethod);
                db.SaveChanges();
                return RedirectToAction("PaymentMethodList");
            }

            return View(paymentMethod);
        }

        //GET: PaymentMethod/Edit/1
        public ActionResult EditPaymentMethod(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            PaymentMethod paymentMethod = db.PaymentMethod.Find(id);
            if (paymentMethod == null)
            {
                return HttpNotFound();
            }
            return View(paymentMethod);
        }

        //POST: PaymentMethod/Edit/1
        [HttpPost]
        public ActionResult EditPaymentMethod([Bind(Include = "Id, Method, EditDate")] PaymentMethod paymentMethod)
        {
            paymentMethod.EditDate = @DateTime.Now;

            if (ModelState.IsValid)
            {
                db.Entry(paymentMethod).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("PaymentMethodList");
            }
            return View(paymentMethod);
        }

        // GET: HotelRating/Delete/1
        public ActionResult DeletePaymentMethod(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            PaymentMethod paymentMethod = db.PaymentMethod.Find(id);
            if (paymentMethod == null)
            {
                return HttpNotFound();
            }
            return View(paymentMethod);
        }

        // POST: PaymentMethod/Delete/1
        [HttpPost]
        public ActionResult DeletePaymentMethod(int id)
        {
            PaymentMethod paymentMethod = db.PaymentMethod.Find(id);
            db.PaymentMethod.Remove(paymentMethod);
            db.SaveChanges();
            return RedirectToAction("PaymentMethodList");
        }

        //GET: RoomAmenity 
        public ActionResult RoomAmenityList()
        {
            return View(db.RoomAmenity.ToList());
        }

        //POST: RoomAmenity/Create 
        [HttpPost]
        public ActionResult CreateRoomAmenity([Bind(Include = "Id, Name, Image, EditDate")] RoomAmenity roomAmenity, HttpPostedFileBase Image)
        {
            roomAmenity.EditDate = @DateTime.Now;

            //if (Image.ContentLength > 0)
            if (Image != null)
            {
                string fileName = Path.GetFileNameWithoutExtension(Image.FileName);
                string extension = Path.GetExtension(Image.FileName);
                fileName = DateTime.Now.ToString("yymmssfff") + fileName + extension;
                roomAmenity.Image = "~/Images/" + fileName;
                string path = Path.Combine(Server.MapPath("~/Images/"), fileName);
                Image.SaveAs(path);
            }

            if (ModelState.IsValid)
            {
                db.RoomAmenity.Add(roomAmenity);
                db.SaveChanges();
                return RedirectToAction("RoomAmenityList");
            }

            return View(roomAmenity);
        }

        //GET: RoomAmenity/Edit/1
        public ActionResult EditRoomAmenity(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            RoomAmenity roomAmenity = db.RoomAmenity.Find(id);
            if (roomAmenity == null)
            {
                return HttpNotFound();
            }
            return View(roomAmenity);
        }

        //POST: RoomAmenity/Edit/1
        [HttpPost]
        public ActionResult EditRoomAmenity([Bind(Include = "Id, Name, Image, EditDate")] RoomAmenity roomAmenity, HttpPostedFileBase Image)
        {
            roomAmenity.EditDate = @DateTime.Now;

            //if (Image.ContentLength > 0)
            if (Image != null)
            {
                string fileName = Path.GetFileNameWithoutExtension(Image.FileName);
                string extension = Path.GetExtension(Image.FileName);
                fileName = DateTime.Now.ToString("yymmssfff") + fileName + extension;
                roomAmenity.Image = "~/Images/" + fileName;
                string path = Path.Combine(Server.MapPath("~/Images/"), fileName);
                Image.SaveAs(path);
            }

            if (ModelState.IsValid)
            {
                db.Entry(roomAmenity).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("RoomAmenityList");
            }
            return View(roomAmenity);
        }

        // GET: RoomAmenity/Delete/1
        public ActionResult DeleteRoomAmenity(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            RoomAmenity roomAmenity = db.RoomAmenity.Find(id);
            if (roomAmenity == null)
            {
                return HttpNotFound();
            }
            return View(roomAmenity);
        }

        // POST: RoomAmenity/Delete/1
        [HttpPost]
        public ActionResult DeleteRoomAmenity(int id)
        {
            RoomAmenity roomAmenity = db.RoomAmenity.Find(id);
            db.RoomAmenity.Remove(roomAmenity);
            db.SaveChanges();
            return RedirectToAction("RoomAmenityList");
        }


        //GET: RoomType 
        public ActionResult RoomTypeList()
        {
            return View(db.RoomType.ToList());
        }

        //POST: RoomType/Create 
        [HttpPost]
        public ActionResult CreateRoomType([Bind(Include = "Id, Name, EditDate")] RoomType roomType)
        {
            roomType.EditDate = @DateTime.Now;

            if (ModelState.IsValid)
            {
                db.RoomType.Add(roomType);
                db.SaveChanges();
                return RedirectToAction("RoomTypeList");
            }

            return View(roomType);
        }

        //GET: RoomType/Edit/1
        public ActionResult EditRoomType(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            RoomType roomType = db.RoomType.Find(id);
            if (roomType == null)
            {
                return HttpNotFound();
            }
            return View(roomType);
        }

        //POST: RoomType/Edit/1
        [HttpPost]
        public ActionResult EditRoomType([Bind(Include = "Id, Name, EditDate")] RoomType roomType)
        {
            roomType.EditDate = @DateTime.Now;         

            if (ModelState.IsValid)
            {
                db.Entry(roomType).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("RoomTypeList");
            }
            return View(roomType);
        }

        // GET: RoomType/Delete/1
        public ActionResult DeleteRoomType(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            RoomType roomType = db.RoomType.Find(id);
            if (roomType == null)
            {
                return HttpNotFound();
            }
            return View(roomType);
        }

        // POST: RoomType/Delete/1
        [HttpPost]
        public ActionResult DeleteRoomType(int id)
        {
            RoomType roomType = db.RoomType.Find(id);
            db.RoomType.Remove(roomType);
            db.SaveChanges();
            return RedirectToAction("RoomTypeList");
        }

        //GET: User 
        public ActionResult UserList()
        {
            return View(db.User.ToList());
        }

        // GET: c/Delete/1
        public ActionResult DeleteUser(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            User user = db.User.Find(id);
            if (user == null)
            {
                return HttpNotFound();
            }
            return View(user);
        }

        // POST: User/Delete/1
        [HttpPost]
        public ActionResult DeleteUser(int id)
        {
            User user = db.User.Find(id);
            db.User.Remove(user);
            db.SaveChanges();
            return RedirectToAction("UserList");
        }

        //GET: Login
        [HttpPost]
        public ActionResult Login(Admin admin)
        {
            var test = db.Admin.Where(x => x.Id == admin.Id && x.Password == admin.Password).FirstOrDefault();          
            if (test == null)
            {
                return View("Index", admin);
            }
            else
            {
                Session["Id"] = admin.Id;
                Session["Email"] = admin.Email;
                return RedirectToAction("AdminDashboard");
            }
            //return View();
        }

        //GET: Logout
        public ActionResult LogOut()
        {
            int Id = (int)Session["Id"];
            Session.Abandon();
            return RedirectToAction("Index");
        }
    }
}