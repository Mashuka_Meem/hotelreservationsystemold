﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace HotelReservationSystem.Models
{
    public class Room
    {
        public int Id { get; set; }

        public Nullable<int> HotelId { get; set; }

        public virtual Hotel Hotel { get; set; }

        public string Description { get; set; }

        public string Image { get; set; }

        public Nullable<int> RoomTypeId { get; set; }

        public virtual RoomType RoomType { get; set; } 

        public Nullable<int> TotalRoom { get; set; }

        public Nullable<int> AvailableRoom { get; set; }  

        public string RoomAmenities { get; set; }

        public Nullable<int> Price { get; set; } 

        public Nullable<bool> Status { get; set; }

        [Display(Name = "Last Edit")]
        public Nullable<DateTime> EditDate { get; set; }

        public virtual ICollection<Reservation> Reservations { get; set; }

        [NotMapped]
        public IEnumerable<RoomType> RoomTypeIds { get; set; }

        [NotMapped]
        public IEnumerable<RoomAmenity> RoomAmenityIds { get; set; }
        [NotMapped]
        public string[] RoomAmenityIdsArray { get; set; }
    }

    //public class RoomList
    //{
    //    IEnumerable<Room> Rooms { get; set; }
    //    [NotMapped]
    //    public IEnumerable<RoomType> RoomTypeIds { get; set; }
    //    [NotMapped]
    //    public string[] RoomTypeIdsArray { get; set; }
    //}
}