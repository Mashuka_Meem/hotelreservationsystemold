﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace HotelReservationSystem.Models
{
    public class HotelAccount
    {
        public int Id { get; set; }
       
        public Nullable<int> PaymentMethodId { get; set; }

        public virtual PaymentMethod PaymentMethod { get; set; } 

        public string AccountNo { get; set; }

        public Nullable<int> HotelId { get; set; }

        public virtual Hotel Hotel { get; set; }

        [Display(Name = "Last Edit")]
        public Nullable<DateTime> EditDate { get; set; }

        public virtual ICollection<Hotel> Hotels { get; set; }

        [NotMapped]
        public IEnumerable<PaymentMethod> PaymentMethodIds { get; set; }
        [NotMapped]
        public string[] PaymentMethodIdsArray { get; set; } 
    }
}