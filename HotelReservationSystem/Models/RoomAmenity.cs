﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace HotelReservationSystem.Models
{
    public class RoomAmenity
    {
        public int Id { get; set; }

        [Display(Name = "Room Amenity")]
        public string Name { get; set; }

        [Display(Name = "Icon")]
        public string Image { get; set; }

        [Display(Name = "Last Edit")]
        public Nullable<DateTime> EditDate { get; set; }

        public virtual ICollection<Room> Rooms { get; set; } 
    }
}