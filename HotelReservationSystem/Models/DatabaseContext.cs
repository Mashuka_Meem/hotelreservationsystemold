﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Data.Entity.ModelConfiguration.Conventions;
using System.Linq;
using System.Web;

namespace HotelReservationSystem.Models
{
    public class DatabaseContext:DbContext
    {
        public DatabaseContext():base("name=HotelReservationSystemDb")
        {

        }

        public DbSet<Actor> Actor { get; set; }

        public DbSet<Admin> Admin { get; set; }

        public DbSet<User> User { get; set; }

        public DbSet<Area> Area { get; set; }

        public DbSet<District> District { get; set; }

        public DbSet<Division> Division { get; set; }

        public DbSet<HotelAmenity> HotelAmenity { get; set; }

        public DbSet<HotelRating> HotelRating { get; set; }

        public DbSet<RoomAmenity> RoomAmenity { get; set; }

        public DbSet<RoomType> RoomType { get; set; }

        public DbSet<PaymentMethod> PaymentMethod { get; set; }

        public DbSet<Hotel> Hotel { get; set; }

        public DbSet<HotelAccount> HotelAccount { get; set; }

        public DbSet<Room> Room { get; set; } 

        public DbSet<Reservation> Reservation { get; set; }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            modelBuilder.Conventions.Remove<ManyToManyCascadeDeleteConvention>();
            modelBuilder.Conventions.Remove<OneToManyCascadeDeleteConvention>();
        }
    }
}