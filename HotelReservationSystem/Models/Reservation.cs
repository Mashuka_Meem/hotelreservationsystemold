﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace HotelReservationSystem.Models
{
    public class Reservation
    {
        public int Id { get; set; }

        public Nullable<int> RoomId { get; set; }

        public virtual Room Room { get; set; }

        [Display(Name = "Check In Date")]
        public Nullable<DateTime> CheckIn { get; set; }

        [Display(Name = "Check Out Edit")]
        public Nullable<DateTime> CheckOut { get; set; }

        public Nullable<int> RoomNo { get; set; } 

        public Nullable<int> UserId { get; set; }

        public virtual User User { get; set; }

        [Display(Name = "Guest Name")]
        public string GuestName { get; set; } 

        [Display(Name = "Guest Contact No")]
        public string Contact { get; set; }

        [Display(Name = "Total Amount")]
        public Nullable<int> TotalAmount { get; set; }

        [Display(Name = "Transaction No")]
        public string TransactionNo { get; set; }

        public Nullable<bool> Status { get; set; }

        [Display(Name = "Reservation Date")]
        public Nullable<DateTime> AddDate { get; set; }

        [Display(Name = "Last Edit")]
        public Nullable<DateTime> EditDate { get; set; }
    }
}