﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace HotelReservationSystem.Models
{
    public class Hotel
    {
        public int Id { get; set; }

        [Display(Name = "Hotel Name")]
        public string Name { get; set; }

        public string Description { get; set; } 

        public string Image { get; set; }

        public string Email { get; set; }

        [Display(Name = "Contact No")]
        public string Contact { get; set; }

        [Display(Name = "Road Address")] 
        public string RoadAddress { get; set; }

        public Nullable<int> AreaId { get; set; }

        public virtual Area Area { get; set; }

        public Nullable<int> DistrictId { get; set; }

        public virtual District District { get; set; }

        public Nullable<int> DivisionId { get; set; }

        public virtual Division Division { get; set; }

        public Nullable<int> HotelRatingId { get; set; }

        public virtual HotelRating HotelRating { get; set; }

        public string HotelAmenities { get; set; }

        public virtual HotelAmenity HotelAmenity { get; set; }

        public string Password { get; set; }

        public Nullable<int> ActorId { get; set; }

        public virtual Actor Actor { get; set; }

        public Nullable<bool> Status { get; set; }

        [Display(Name = "Joining Date")]
        public Nullable<DateTime> JoinDate { get; set; }

        [Display(Name = "Last Edit")]
        public Nullable<DateTime> EditDate { get; set; }

        public virtual ICollection<HotelAccount> HotelAccounts { get; set; }

        public virtual ICollection<Room> Rooms { get; set; }


        [NotMapped] 
        public IEnumerable<HotelAmenity> HotelAmenityIds { get; set; }

        [NotMapped]
        public string[] HotelAmenityIdsArray { get; set; }

        [NotMapped]
        public IEnumerable<HotelRating> HotelRatingIds { get; set; }


        [NotMapped]
        public IEnumerable<Division> DivisionIds { get; set; }

        [NotMapped]
        public IEnumerable<District> DistrictIds { get; set; }

        [NotMapped]
        public IEnumerable<Area> AreaIds { get; set; }
        
    }
}