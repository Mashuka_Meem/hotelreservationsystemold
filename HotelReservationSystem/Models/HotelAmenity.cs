﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace HotelReservationSystem.Models
{
    public class HotelAmenity
    {
        public int Id { get; set; }

        [Display(Name = "Hotel Amenity")]
        public string Name { get; set; }

        [Display(Name = "Icon")]
        public string Image { get; set; } 

        [Display(Name = "Last Edit")]
        public Nullable<DateTime> EditDate { get; set; }

        public virtual ICollection<Hotel> Hotels { get; set; }
    }
}