﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;

namespace HotelReservationSystem.Models
{
    public class HotelsController : Controller
    {
        private DatabaseContext db = new DatabaseContext();

        // GET: Hotels
        public ActionResult Index()
        {
            var hotel = db.Hotel.Include(h => h.Actor).Include(h => h.Area).Include(h => h.District).Include(h => h.Division).Include(h => h.HotelRating);
            return View(hotel.ToList());
        }

        // GET: Hotels/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Hotel hotel = db.Hotel.Find(id);
            if (hotel == null)
            {
                return HttpNotFound();
            }
            return View(hotel);
        }

        // GET: Hotels/Create
        public ActionResult Create()
        {
            ViewBag.ActorId = new SelectList(db.Actor, "Id", "Type");
            ViewBag.AreaId = new SelectList(db.Area, "Id", "Name");
            ViewBag.DistrictId = new SelectList(db.District, "Id", "Name");
            ViewBag.DivisionId = new SelectList(db.Division, "Id", "Name");
            ViewBag.HotelRatingId = new SelectList(db.HotelRating, "Id", "Rating");
            return View();
        }

        // POST: Hotels/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "Id,Name,Description,Image,Email,Contact,RoadAddress,AreaId,DistrictId,DivisionId,HotelRatingId,HotelAmenities,Password,ActorId,Status,JoinDate,EditDate")] Hotel hotel)
        {
            if (ModelState.IsValid)
            {
                db.Hotel.Add(hotel);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            ViewBag.ActorId = new SelectList(db.Actor, "Id", "Type", hotel.ActorId);
            ViewBag.AreaId = new SelectList(db.Area, "Id", "Name", hotel.AreaId);
            ViewBag.DistrictId = new SelectList(db.District, "Id", "Name", hotel.DistrictId);
            ViewBag.DivisionId = new SelectList(db.Division, "Id", "Name", hotel.DivisionId);
            ViewBag.HotelRatingId = new SelectList(db.HotelRating, "Id", "Rating", hotel.HotelRatingId);
            return View(hotel);
        }

        // GET: Hotels/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Hotel hotel = db.Hotel.Find(id);
            if (hotel == null)
            {
                return HttpNotFound();
            }
            ViewBag.ActorId = new SelectList(db.Actor, "Id", "Type", hotel.ActorId);
            ViewBag.AreaId = new SelectList(db.Area, "Id", "Name", hotel.AreaId);
            ViewBag.DistrictId = new SelectList(db.District, "Id", "Name", hotel.DistrictId);
            ViewBag.DivisionId = new SelectList(db.Division, "Id", "Name", hotel.DivisionId);
            ViewBag.HotelRatingId = new SelectList(db.HotelRating, "Id", "Rating", hotel.HotelRatingId);
            return View(hotel);
        }

        // POST: Hotels/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "Id,Name,Description,Image,Email,Contact,RoadAddress,AreaId,DistrictId,DivisionId,HotelRatingId,HotelAmenities,Password,ActorId,Status,JoinDate,EditDate")] Hotel hotel)
        {
            if (ModelState.IsValid)
            {
                db.Entry(hotel).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            ViewBag.ActorId = new SelectList(db.Actor, "Id", "Type", hotel.ActorId);
            ViewBag.AreaId = new SelectList(db.Area, "Id", "Name", hotel.AreaId);
            ViewBag.DistrictId = new SelectList(db.District, "Id", "Name", hotel.DistrictId);
            ViewBag.DivisionId = new SelectList(db.Division, "Id", "Name", hotel.DivisionId);
            ViewBag.HotelRatingId = new SelectList(db.HotelRating, "Id", "Rating", hotel.HotelRatingId);
            return View(hotel);
        }

        // GET: Hotels/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Hotel hotel = db.Hotel.Find(id);
            if (hotel == null)
            {
                return HttpNotFound();
            }
            return View(hotel);
        }

        // POST: Hotels/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            Hotel hotel = db.Hotel.Find(id);
            db.Hotel.Remove(hotel);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
