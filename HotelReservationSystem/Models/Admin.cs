﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace HotelReservationSystem.Models
{
    public class Admin
    {
        public int Id { get; set; }
   
        public string Name { get; set; }

        public string Image { get; set; }

        public string Email { get; set; }

        [Display(Name = "Contact No")]
        public string Contact { get; set; }
        
        public string Password { get; set; }

        public Nullable<int> ActorId { get; set; }

        public virtual Actor Actor { get; set; }

        public Nullable<bool> Status { get; set; }

        [Display(Name = "Joining Date")]
        public Nullable<DateTime> JoinDate { get; set; } 

        [Display(Name = "Last Edit")]
        public Nullable<DateTime> EditDate { get; set; }
    }
}