﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace HotelReservationSystem.Models
{
    public class Actor
    {
        public int Id { get; set; }
        
        [Display(Name ="Actor Type")]
        public string Type { get; set; }

        [Display(Name = "Last Edit")]
        public Nullable<DateTime> EditDate { get; set; }

        public virtual ICollection<Admin> Admins { get; set; }

        public virtual ICollection<Hotel> Hotels { get; set; }

        public virtual ICollection<User> Users { get; set; }
    }
}